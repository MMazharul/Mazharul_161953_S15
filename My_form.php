<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>creat_php</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<div class="col-md-2"></div>
<form action="process.php" method="POST">
    <div class="col-md-8">
    <div class="form-group">
        <label for="first">FirstName</label>
        <input type="text" class="form-control" id="first" name="first" placeholder="firstname">
    </div>
    <div class="form-grup">
        <label for="last">LastName</label>
        <input type="text" class="form-control" id="last" name="second" placeholder="lastname">
    </div>
    <div class="form-group">
        <label for="id">Id</label>
        <input type="number" class="form-control" id="id" name="id" placeholder="id">
    </div>
    <div class="form-group">
        <label for="bl">Blood</label>
        <input type="text" class="form-control" id="bl" name="bl" placeholder="Blood">
    </div>
    <button type="submit" class="btn btn-success">Submit</button>
    </div>
</form>
<div class="col-md-2"></div>
</body>
</html>